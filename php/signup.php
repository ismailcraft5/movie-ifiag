<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

$servername = "localhost";
$username = "root";
$password = "azeqsd2022";
$dbname = "moviedata";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $data = json_decode(file_get_contents("php://input"));

    $username = $data->username;
    $email = $data->email;
    $password = $data->password;

    $stmt = $conn->prepare("SELECT * FROM tbl_signup WHERE username = :username");
    $stmt->bindParam(":username", $username);
    $stmt->execute();
    $exist = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($exist) {
        http_response_code(400);
        echo json_encode(array("message" => "UserName already exist"));
    } else {
        $stmt = $conn->prepare("INSERT INTO tbl_signup (username, email, pass) VALUES (:username, :email, :password)");
        $stmt->bindParam(":username", $username);
        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":password", $password);
        $stmt->execute();

        http_response_code(201); 
        echo json_encode(array("message" => "New User Successfully Added"));
    }
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

?>
