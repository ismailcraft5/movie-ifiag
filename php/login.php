<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Accept");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

$servername = "localhost";
$username = "root";
$password = "azeqsd2022";
$dbname = "moviedata";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die("Connection failed: " . $e->getMessage()); 
}

$data = json_decode(file_get_contents("php://input"));
$error = null;

if (!isset($data->username) || !isset($data->password)) {
    $error = "not found in database";
} else {
    $username = $data->username;
    $password = $data->password;
    
    $query = $conn->prepare('SELECT password FROM tbl_signup WHERE username = :username');
    $query->execute(array(':username' => $username));
    $result = $query->fetch();

    if ($result) {
        if (password_verify($password, $result['password'])) {
            http_response_code(200);
        } else {
            http_response_code(400);
            $error = "Invalid password";
        }
    } else {
        http_response_code(404);
        $error = "failed";
    }
}

if ($error) {
    echo json_encode(array("message" => $error));
}
?>
