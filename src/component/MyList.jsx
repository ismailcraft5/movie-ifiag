import React from "react";
import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

const MyList = () => {
  const { media_type } = useParams();
  const url = `https://image.tmdb.org/t/p/original`;
  const pStyle = {
    fontSize: "15px",
    color: "white",
  };
  const [watchlist, setWatchlist] = useState([]);

  useEffect(() => {
    const storedWatchlist = JSON.parse(localStorage.getItem("watchlist"));
    if (storedWatchlist) {
      setWatchlist(storedWatchlist);
    }
  }, []);

  const toggleWatchlist = (movie) => {
    if (watchlist.some((m) => m.id === movie.id)) {
      // If movie is already in watchlist, remove it
      removeFromWatchlist(movie);
    } else {
      // If movie is not in watchlist, add it
      addToWatchlist(movie.id);
    }
  };
  const addToWatchlist = (movieId) => {
    return fetch(
      `https://api.themoviedb.org/3/${media_type}/${movieId}?api_key=08399bf740a4d93d9e75e8a3a6917e88&language=en-US`
    )
      .then((response) => response.json())
      .then((data) => {
        const newMovie = data;
        setWatchlist((prevWatchlist) => {
          const newWatchlist = [...prevWatchlist, newMovie];
          localStorage.setItem("watchlist", JSON.stringify(newWatchlist));
          return newWatchlist;
        });
        return newMovie;
      })
      .catch((error) => console.log(error));
  };
  

  const removeFromWatchlist = (movie) => {
    setWatchlist((prevWatchlist) => {
      const newWatchlist = prevWatchlist.filter((m) => m.id !== movie.id);
      localStorage.setItem("watchlist", JSON.stringify(newWatchlist));
      return newWatchlist;
    });
  };

  return (
    <div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <h2 className="text-white">My List</h2>
      {watchlist.length > 0 ? (
        <ul>
          {watchlist.map((movie) => (
            <li style={pStyle} key={movie.id}>
              <img
                className="shadow-2xl md:h-[360px] h-52 mt-14 mb-3   object-cover bg-right-bottom  md:w-[300px]  w-36   rounded-md  md:mx-10 mx-0"
                src={url + movie.poster_path}
                alt=""
              />
              {movie.title}{" "}
              <button
                className="ml-10 bg-red-900 rounded p-2"
                onClick={() => toggleWatchlist(movie)}
              >
                {watchlist.some((m) => m.id === movie.id) ? "Remove" : "Add"}
              </button>
            </li>
          ))}
        </ul>
      ) : (
        <p className="text-white">No movies in your watchlist yet.</p>
      )}
    </div>
  );
};
export default MyList;
