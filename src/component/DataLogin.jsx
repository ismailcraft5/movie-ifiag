import React from 'react';
import { Link } from 'react-router-dom';
import { TextField } from './TextField';
import { Formik, Form } from 'formik';
import axios from 'axios';


const DataLogin =()=> {
    

  return (
<Formik
  initialValues={{
    username: '',
    password: '',
  }}
  onSubmit={(values, actions) => {
   axios.post('https://thawing-waters-55493.herokuapp.com/', {
      url: 'http://localhost:8080/php/login.php',
      method: 'post',
      data: {
        username: values.username,
        password: values.password
      },
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then(response => {
      console.log(response);
      if(response.data.status === 'success') {
        alert('Welcome');
      } else {
        alert('Invalid info');
        console.log('Failed');
      }
      actions.setSubmitting(false);
    })
    .catch(error => {
      console.log(error);
      alert('Invalid info');
      console.log('Failed');
      actions.setSubmitting(false);
    });

     
  }}
>

            {Formik=>(
                    <Form action="" className="sm:w-2/3 w-full px-4 lg:px-0 mx-auto">
                    <div className="pb-2 pt-4">
                        <TextField type="text" name="username" id="username" placeholder="username" className=" block w-full p-4 text-lg rounded-sm bg-black"/>
                    </div>
                    <div className="pb-2 pt-4">
                        <TextField className="block w-full p-4 text-lg rounded-sm bg-black" type="password" name="password" id="password" placeholder="Password"/>
                    </div>
                    <div className="text-right text-gray-400 hover:underline hover:text-gray-100">
                    
                        <Link to="/Register" className='text-red-600'>Create an account !</Link> 
                    </div>
                    <div className="px-4 pb-2 pt-4">
                        <button type='submit' className="uppercase block w-full p-4 text-lg rounded-full bg-red-800 hover:bg-red-900 focus:outline-none">LOGIN</button>
                    </div>
                  </Form>
            )}  
    </Formik>
  )
}
export default DataLogin;