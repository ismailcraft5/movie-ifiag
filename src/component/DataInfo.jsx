import React from "react";
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { TextField } from "./TextField";

export const DataInfo = () => {
  const validate = Yup.object({
    username: Yup.string()
      .max(15, 'Must be 15 characters or less')
      .required('Required'),
    password: Yup.string()
      .min(6, 'Password must be at least 6 characters')
      .required('Password is required'),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref('password'), null], 'Password must match')
      .required('Confirm password is required'),
    email: Yup.string()
      .email('Email is invalid')
      .required('Email is required'),
  });

  const navigateToLogin = () => {
    window.location.href = "/Login";
  };
  
  return (
    <Formik
      initialValues={{
        username: '',
        email: '',
        password: '',
        confirmPassword: ''
      }}
      validationSchema={validate}
      onSubmit={data => {
        console.log(data);

        axios.post('http://localhost:8080/php/signup.php',{
          username: data.username,
          email: data.email,
          password: data.password
        }, {
          headers: {
            'Content-Type': 'application/json'
          }
        })
        .then(function (response) {
          console.log(response);
          alert('New User Successfully Added.');  
          navigateToLogin();
        })
        .catch(function (response) {
          console.log(response);
          if(response.status === 400){
            
          } else {
            alert("UserName already exist");
            console.log("USER WAS NOT ADDED");
          }
        });
      }}>
      {formik => (
        <div className="flex flex-col rounded-lg px-8 py-10 w-full bg-black bg-opacity-75 md:bg-opacity-100">
          <Form className="flex flex-col space-y-8">
            <TextField className=" block w-full p-4 text-lg  bg-black border" placeholder="Enter username" name="username" type="text" />
            <TextField className="block w-full p-4 text-lg  bg-black border" placeholder="Enter email" name="email" type="email" />
            <TextField className="block w-full p-4 text-lg  bg-black border " placeholder="Password" name="password" type="password" />
            <TextField className="block w-full p-4 text-lg  bg-black border" placeholder="Confirm password" name="confirmPassword" type="password" />
            <button className="uppercase block w-full p-4 text-lg rounded-full bg-red-800 hover:bg-red-900 focus:outline-none" type="submit">Register</button>
          </Form>
        </div>
      )}
    </Formik>
  );
};

export default DataInfo;
