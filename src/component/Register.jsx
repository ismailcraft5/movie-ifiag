import React from 'react';
import loginCover from '../assets/loginCover.png';
import DataInfo from './DataInfo';


 const Login=()=> {
  return (
    <div>

    <section className="min-h-screen flex items-stretch text-white ">
        <div className="lg:flex w-1/2 hidden bg-gray-500 bg-no-repeat bg-cover relative items-center" style={{backgroundImage:`url(${loginCover})`}}>
            <div className="absolute bg-black opacity-60 inset-0 z-0"></div>
            <div className="w-full px-24 z-10">
                <h1 className="text-5xl font-bold text-left tracking-wide">WELCOME</h1>
                <p className="text-3xl my-4">Create your account to start watch your favorites movies</p>
            </div>
        </div>
        <div className="lg:w-1/2 w-full flex items-center justify-center text-center md:px-16 px-0 z-0 bg-black" >
            <div className="absolute lg:hidden z-10 inset-0 bg-gray-500 bg-no-repeat bg-cover items-center" style={{backgroundImage:`url(${loginCover})`}}>
                <div className="absolute bg-black opacity-60 inset-0 z-0"></div>
            </div>
            <div className="w-full py-6 z-20">
               
                <DataInfo/>
            </div>
        </div>
    </section>
    </div>
  )
};
export default Login;
